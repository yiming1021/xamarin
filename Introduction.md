## 目錄

1. [訊息推播](/push_notification_service.md)
1. Xamarin.Forms

### Xamarin.Forms Controls Reference [link](https://developer.xamarin.com/guides/xamarin-forms/controls/)

> Xamarin.Forms Pages & Layouts

**Pages**

![alt Xamarin.Forms Pages](https://developer.xamarin.com/guides/xamarin-forms/controls/pages/Images/Pages-sml.png)

**Layouts**

![alt Xamarin.Forms Layouts](https://developer.xamarin.com/guides/xamarin-forms/controls/layouts/Images/Layouts-sml.png)


### Q&A

使用 Xamarin 解決方案需要有 Xamarin 授權。

使用 Xamarin, React Native, 或其它框架開發跨平台 APP，建議搭載 Windows 8+ 和 Mac mini 作為完整開發環境。

不管用什麼解決方案，在開發結束後如果要發佈為 APP 都需要原生的憑證作為發佈的內容之一。
